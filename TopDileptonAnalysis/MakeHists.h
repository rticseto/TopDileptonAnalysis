#ifndef TOPANALYSIS_MAKEHISTS_H_
#define TOPANALYSIS_MAKEHISTS_H_

#include <string>
#include <unordered_map>

#include "TFile.h"
#include "TH2D.h"
#include "TDirectory.h"

namespace top {

class MakeHists {
 public:
  MakeHists(TFile* outputfile, std::string systematic_name)
    : m_outfile(outputfile), m_name(systematic_name) {

    TDirectory* dir = outputfile->mkdir(systematic_name.c_str());
    dir->cd();
    m_hists["migr_top_pt"] = new TH2D("migr_top_pt", "migr_top_pt", 10, 0., 1000., 10, 0., 1000.); //update this to correct bins

    ;}
  ~MakeHists() { ;}

  TFile* m_outfile;
  std::string m_name;
  std::unordered_map<std::string, TH2D*> m_hists;
};
}  // namespace top

#endif  // TOPANALYSIS_MAKEHISTS_H_
